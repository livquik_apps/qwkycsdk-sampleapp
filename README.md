# SDK Integration - Android

To get up and running Full Video KYC process on an Android app, please follow the step-by-step
implementation of QW-KYC SDK.

#### Configuring your QW-KYC Account
LQ integration team will provide a unique **PROGRAM_ID** and **CLIENT_KEY** to an integrating client app.
This ID is required for LQ to identify your application. LQ integration team will also provide login
credentials for agent and auditor dashboard. All these account details will be different for the test and
live environment.
More details on prog ID and secret provided by LQ,
**PROGRAM_ID:** It's an unique identifier for each partner integrating the VKYC SDK/APIs
**CLIENT_KEY:** Secret key provided by the VKYC vendor for differentiating between different programs.

#### SDK Integration and adding dependencies
1. Open your project AndroidManifest.xml and add permissions for Internet
```sh
<uses-permission android:name="android.permission.ACCESS_NETWORK_STATE" />
<uses-permission android:name="android.permission.INTERNET" />
```
2. Open your app level build.gradle file, add the below dependencies, and sync your project
```sh 
implementation 'com.livquik.qwkycsdk:qwkycsdk:1.0.1'
```

3. Add repository url at project level build.gradle file
```sh 
allprojects {
    repositories {
        google()
        mavenCentral()
        maven {
            credentials {
                username "USER_NAME" // Will provide by LQ Team
                password "PASSWORD" // Will provide by LQ Team
            }
            authentication {
                basic(BasicAuthentication)
            }
            url "REPO_URL" // Will provide by LQ Team
        }
    }
}
```
#### SDK Initialisation for Full KYC process
Before initializing the QW-KYC SDK, add the below mentioned 3 configurations to set the environment,
client key and KYC verification type.
```sh 
QwKYC.INSTANCE.isStaging(true | false);
QwKYC.INSTANCE.setClientKey([CLIENT_KEY]);
QwKYC.INSTANCE.setKycVerificationType(QwKYC.KYC_VERIFICATION_TYPE.ONLY_VCIP);
```

Parameters specifications

| Parameters | Required | Description |
| ------ | ------ | ------ |
| isStaging | M | Set, True for **staging env** And  False for **live env.** |
| Client Key  | M | Set Client key shared by LQ integration team |
| KYC Verification Type  | M | Set one of the below mentioned types full KYC verification.
|||● **AADHAAR_PAN_VCIP**
|||● **AADHAAR_VCIP**
|||● **PAN_VCIP**
|||● **ONLY_VCIP**

After setting the above configuration, you can initiate the SDK with the below method.
```sh 
InitiateKYC.Builder initiateKYC = new InitiateKYC.Builder(this);
initiateKYC
.mobileNo([mobile number])
.programId([program id])
.m2pToken([m2pToken])
.firstName([first name])
.middleName([middle name])
.lastName([lastname])
.fatherName([father's name])
.motherName([mother's name])
.spouseName([spouse's name])
.emailId([email id])
.userdob([Date of Birth])
.gender([male|female|other])
.currentAddress([current address])
.permanentAddress([permanent address])
.panNumber([PAN number])
.occupationType([occupation type])
.customerType([customer type])
.aadhaarLastdigits([last 4 digits of aadhaar])
.orderId[order ID]
.ckycNumber([CKYC ID])
.genDate([generation date])
.fileBase64([aadhaar face file base64 format])
.isCurrentAndPermanentAddressSame([true|false])
.build();
```
All of the above mentioned status checks are handled by QW-KYC SDK. Client app will receivenappropriate status and message in **onActivityResult** method.

```sh 
@Override
protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
super.onActivityResult(requestCode, resultCode, data);
    if (data != null) {
        Bundle s = data.getExtras();
        String kycStatus = s.getString("kyc_status");
        String status = s.getString("status");
        String message = s.getString("message");
        
        [if condition to check if bundle has keys "appointment_link" & "appointment_time"] {
            String appointmentLink = s.getString("appointment_link");
            String appointmentTime = s.getString("appointment_time");
        }
        // TODO : client app flow
    }
}
```
Client app can also fetch the current status of user, anywhere inside the app, by calling getKycStatus method of QW-KYC SDK as mentioned below:
```sh
QwKYC.INSTANCE.getKycStatus(mobileNo, programId, new QwKYC.OnKycStatusListener() {
    @Override
    public void onGetKycStatusFailed(@NonNull String message) {
        // TODO : client app flow
    }
    @Override
    public void onGetKycStatusSuccess(@NonNull KycStatus kycStatus) {
    // TODO : client app flow
    // KYC Category : kycStatus.getKycCategory()
    // Wallet Status : kycStatus.getWalletStatus()
    }
});
```
##### Implementing Login / Logout mechanism for the User
To logout the user from the SDK, please use below line of code:
```sh
QwKYC.INSTANCE.logout(this);
```
To check the login status of the user, please use below line of code:
```sh
QwKYC.INSTANCE.isLogin(this)
This function will return boolean value.
If the value returned is true, then you can also fetch the mobile
number and the program Id used for the KYC process using below lines
of code:
QwKYC.INSTANCE.getMobile(this)
QwKYC.INSTANCE.getProgramId(this)
```
##### Listening to QW-Events during Video call process:
Client apps can listen to the events received in the SDK during the video call process by calling
setQwEventListener method of QW-KYC SDK as mentioned below. The UI customizations on the client
app can be done using these events.
```sh
initiateKYC.setQwEventListener((QwEventListener) jsonString -> {
    Log.i("QwEventListener", jsonString);
    // TODO : implement your customisations
});
```