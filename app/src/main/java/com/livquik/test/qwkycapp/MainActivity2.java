package com.livquik.test.qwkycapp;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.messaging.FirebaseMessaging;
import com.livquik.listener.QwEventListener;
import com.livquik.model.KycStatus;
import com.livquik.sdk.InitiateKYC;
import com.livquik.sdk.QwKYC;
import com.livquik.utils.QwKycConstant;

-import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;

public class MainActivity2 extends AppCompatActivity {

    Button initiateKycBtn;
    EditText mobileEdt, programidEdt;

    String mobileNo = "", programId = "";
    int kycType = 0;
    ArrayList<String> list = new ArrayList<>();
    HashMap<String, Integer> listMap = new HashMap<String, Integer>();

    InitiateKYC.Builder initiateKYC = null;
    private ConstraintLayout mainCl;
    private Button kycStatusBtn;
    private Spinner kycSpinner;
    private View progressBar;
    private Button logoutBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mainCl = findViewById(R.id.main_cl);
        progressBar = findViewById(R.id.progress_bar);

        mobileEdt = findViewById(R.id.edt_mobile);
        programidEdt = findViewById(R.id.edt_programid);
        initiateKycBtn = findViewById(R.id.btn_initiate_kyc);
        kycStatusBtn = findViewById(R.id.btn_kyc_status);
        logoutBtn = findViewById(R.id.btn_logout);
        kycSpinner = findViewById(R.id.kyc_spinner);

        list.add("Aadhaar PAN & VCIP");
        list.add("Aadhaar & VCIP");
        list.add("PAN & VCIP");
        list.add("Only VCIP");

        listMap.put("Aadhaar PAN & VCIP", 1);
        listMap.put("Aadhaar & VCIP", 2);
        listMap.put("PAN & VCIP", 3);
        listMap.put("Only VCIP", 4);

        ArrayAdapter adapter = new ArrayAdapter(this, R.layout.support_simple_spinner_dropdown_item, list);
        kycSpinner.setAdapter(adapter);

        setListeners();

        initiateKYC = new InitiateKYC.Builder(this);

        if (QwKYC.INSTANCE.isLogin(this)) {
            mobileEdt.setText(QwKYC.INSTANCE.getMobile(this));
            programidEdt.setText(QwKYC.INSTANCE.getProgramId(this));
        }

        //Enable test/prod environment
        QwKYC.INSTANCE.isStaging(true);
        QwKYC.INSTANCE.setClientKey("UPKJsoHAdQtL6jBXWZu5");      // test
//        QwKYC.INSTANCE.setClientKey("ppUEUfSU4VQ5dsg");             // prod
        QwKYC.INSTANCE.setKycVerificationType(QwKYC.KYC_VERIFICATION_TYPE.ONLY_VCIP);

        initiateKYC.setQwEventListener((QwEventListener) jsonString -> {
            Log.i("QwEventListener", jsonString);
        });

        FirebaseMessaging.getInstance().getToken()
                .addOnCompleteListener(new OnCompleteListener<String>() {
                    @Override
                    public void onComplete(@NonNull Task<String> task) {
                        if (!task.isSuccessful()) {
                            Log.w("TAG", "Fetching FCM registration token failed", task.getException());
                            return;
                        }

                        // Get new FCM registration token
                        String token = task.getResult();
                        if (token != null) {
                            QwKYC.INSTANCE.registerFcmToken(MainActivity2.this, token);
                        }
                    }
                });

    }

    private void setListeners() {

        kycSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                kycType = listMap.get(parent.getItemAtPosition(position).toString());
                QwKYC.INSTANCE.setKycVerificationType(kycType);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        initiateKycBtn.setOnClickListener(v -> {
            if (validateFields()) {
                int kycType = listMap.get(list.get(kycSpinner.getSelectedItemPosition()));
                switch (kycType) {
                    case QwKYC.KYC_VERIFICATION_TYPE.AADHAAR_PAN_VCIP:
                        initiateKYC
                                .mobileNo(mobileNo)
                                .programId(Integer.parseInt(programId))
                                .m2pToken("test-token")
                                .firstName("John")
                                .lastName("Doe")
                                .emailId("johndoe@gmail.com")
                                .userdob("10/05/1987")
                                .gender("male")
                                .currentAddress("Mumbai")
                                .permanentAddress("Mumbai")
                                .isCurrentAndPermanentAddressSame(true)
                                .build();
                        break;
                    case QwKYC.KYC_VERIFICATION_TYPE.AADHAAR_VCIP:
                        initiateKYC
                                .mobileNo(mobileNo)
                                .programId(Integer.parseInt(programId))
                                .m2pToken("test-token")
                                .firstName("John")
                                .lastName("Doe")
                                .emailId("johndoe@gmail.com")
                                .userdob("10/05/1987")
                                .gender("male")
                                .currentAddress("Mumbai")
                                .permanentAddress("Mumbai")
                                .isCurrentAndPermanentAddressSame(true)
                                .panNumber("DCVPM4582S")
                                .genDate(getGenDate())
                                .build();
                        break;
                    case QwKYC.KYC_VERIFICATION_TYPE.PAN_VCIP:
                        initiateKYC
                                .mobileNo(mobileNo)
                                .programId(Integer.parseInt(programId))
                                .m2pToken("test-token")
                                .firstName("John")
                                .lastName("Doe")
                                .emailId("johndoe@gmail.com")
                                .userdob("10/05/1987")
                                .gender("male")
                                .currentAddress("Mumbai")
                                .permanentAddress("Mumbai")
                                .isCurrentAndPermanentAddressSame(true)
                                .aadhaarLastdigits("9873")
                                .fileBase64("testfile")
                                .genDate(getGenDate())
                                .build();
                        break;
                    case QwKYC.KYC_VERIFICATION_TYPE.ONLY_VCIP:
                        initiateKYC
                                .mobileNo(mobileNo)
                                .programId(Integer.parseInt(programId))
                                .m2pToken("test-token")
                                .firstName("John")
                                .lastName("Doe")
                                .emailId("johndoe@gmail.com")
                                .userdob("10/05/1987")
                                .gender("male")
                                .currentAddress("Mumbai")
                                .permanentAddress("Mumbai")
                                .isCurrentAndPermanentAddressSame(true)
                                .panNumber("DCVPM4582S")
                                .aadhaarLastdigits("9873")
                                .fileBase64("testfile")
                                .genDate(getGenDate())
                                .build();
                        break;
                    default:
                        showSnackbar("Please select KYC type.");
                }
            }
        });

        kycStatusBtn.setOnClickListener(v -> {
            if (validateFields()) {
                showProgressBar();
                QwKYC.INSTANCE.getKycStatus(mobileNo, Integer.parseInt(programId), new QwKYC.OnKycStatusListener() {
                    @Override
                    public void onGetKycStatusFailed(@NonNull String message) {
                        hideProgressBar();
                        showSnackbar(message);
                    }

                    @Override
                    public void onGetKycStatusSuccess(@NonNull KycStatus kycStatus) {
                        hideProgressBar();
                        showSnackbar("KycCategory : " +
                                kycStatus.getKycCategory() +
                                " WalletStatus : " +
                                kycStatus.getWalletStatus());
                    }
                });
            }
        });

        logoutBtn.setOnClickListener(v -> {
            QwKYC.INSTANCE.logout(this);
            mobileEdt.setText("");
            programidEdt.setText("");
        });
    }

    private boolean validateFields() {

        mobileNo = mobileEdt.getText().toString();
        programId = programidEdt.getText().toString();

        if (mobileNo.isEmpty()) {
            mobileEdt.setError("Please enter valid mobile number");
            return false;
        } else if (programId.isEmpty()) {
            programidEdt.setError("Please enter valid program id");
            return false;
        } else if (kycType == 0) {
            showSnackbar("Please select KYC Verification Type");
            return false;
        } else {
            mobileEdt.setError(null);
            programidEdt.setError(null);
            return true;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (data != null && requestCode == QwKycConstant.SdkConstants.INITIALIZE_RESULT_CODE) {
            Bundle s = data.getExtras();
            String status = s.getString("status");
            String message = s.getString("message");
            if (message.equalsIgnoreCase("Your Appointment is Scheduled.")) {
                String appointmentLink = s.getString("appointment_link");
                String appointmentTime = s.getString("appointment_time");
                Log.v("Data Appointment >>>>>", appointmentLink + " " + appointmentTime);
            }
            showSnackbar(message);
        }
    }

    private void showSnackbar(String message) {
        Snackbar.make(mainCl, message, Snackbar.LENGTH_LONG).show();
    }

    //This date for test purpose. please follow integration doc for gen date.
    private String getGenDate() {
        SimpleDateFormat sdf = new SimpleDateFormat("YYYYMMddHHmmSS", Locale.getDefault());
        return sdf.format(new Date());
    }


    private void showProgressBar() {
        progressBar.setVisibility(View.VISIBLE);
        initiateKycBtn.setVisibility(View.INVISIBLE);
        kycStatusBtn.setVisibility(View.INVISIBLE);
    }

    private void hideProgressBar() {
        progressBar.setVisibility(View.GONE);
        initiateKycBtn.setVisibility(View.VISIBLE);
        kycStatusBtn.setVisibility(View.VISIBLE);
    }
}